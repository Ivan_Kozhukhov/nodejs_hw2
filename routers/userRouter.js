const express = require('express');
const router = express.Router();
const { getUser, deleteUser, changeUserPassword } = require('../controllers/usersController');

router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch('/me', changeUserPassword);

module.exports = router;