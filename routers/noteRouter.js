const express = require('express');
const router = express.Router();
const { createNote, getNotes, getNote, updateNote, updateCompletedValue, deleteNote } = require('../controllers/noteController');
const { noteExistenceMiddleware } = require('../middlewares/noteExistenceMiddleware');

router.post('', createNote);
router.get('', getNotes);
router.get('/:id', noteExistenceMiddleware, getNote);
router.put('/:id', noteExistenceMiddleware, updateNote);
router.patch('/:id', noteExistenceMiddleware, updateCompletedValue);
router.delete('/:id', noteExistenceMiddleware, deleteNote);

module.exports = router; 