const { Note } = require('../models/noteModel'); 

module.exports.createNote = async (req, res, next) => {
    try {
        const { text } = req.body;
        const { id } = res.locals;
        const note = new Note({
            text,
            userId: id
        });
        await note.save();
        res.status(200).json({message: "Success"});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.getNotes = async (req, res, next) => {
    try {
        const { id } = res.locals;
        const { offset = 0, limit = 5 } = req.query;
        const notes = await Note.find({ userId: id }, {__v: 0,}, { skip: parseInt(offset), limit: parseInt(limit)});
        res.status(200).json({
            offset: parseInt(offset),
            limit: parseInt(limit),
            count: notes.length,
            notes
        });
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.getNote = async (req, res, next) => {
    try {
        const { id } = req.params;
        const note = await Note.findOne({ _id: id }, {__v: 0});
        res.status(200).json({note});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.updateNote = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { text } = req.body;
        await Note.findByIdAndUpdate( id , { text });
        res.status(200).json({message:'Success'});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.updateCompletedValue = async (req, res, next) => {
    try {
        const { id }  = req.params;
        const note = await Note.findById( id );
        note.completed = !note.completed;
        await note.save();
        res.status(200).json({message:'Success'});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.deleteNote = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Note.findByIdAndDelete(id);
        res.status(200).json({message: 'Success'});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};