const { User } = require('../models/userModel'); 
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const saltRound = bcrypt.genSaltSync(10);
const { SECRET_WORD } = require('../config'); 

module.exports.login = async (req, res, next) => {
    try {
        const {username, password} = req.body;
        const user = await User.findOne({username});
        if(!user) {
            res.status(400).json({message: `User with ${username} username is not exist`});
        }
        const validPassword = bcrypt.compare(password, user.password);
        if(!validPassword) {
            res.status(400).json({message: 'Wrong password! Please try again'});
        }
        const token = jwt.sign({username: user.username, id: user._id}, SECRET_WORD, {expiresIn: '24h'});
        return res.status(200).json({message: "Success", jwt_token: `${token}`});
    } catch (e) {
        res.status(400).json({message: "login error"});
    }
};


module.exports.register = async (req, res, next) => {
    try {
        const {username, password} = req.body;
        const candidate = await User.findOne({username});
        if(candidate) {
            return res.status(400).json({message: `username: ${username} is already taken`});
        }
        const user = new User({
            username,
            password: await bcrypt.hashSync(password, saltRound)
            
        });
        await user.save();
        res.status(200).json({message: "Success"});
    } catch (e) {
        res.status(400).json({message: "registration error"})
    }
};