const { User } = require('../models/userModel'); 
const bcrypt = require('bcryptjs');
const saltRound = bcrypt.genSaltSync(10);

module.exports.getUser = async (_, res) => {
    try {
        const { id } = res.locals;
        const user = await User.findById(id);
        const userData = {user: {
            _id: user.id,
            username: user.username,
            createdDate: user.createdDate
        }};
        res.status(200).json(userData);
    } catch (e) {
        res.status(400).json({message: e.message})
    }
};

module.exports.deleteUser = async (_, res) => {
    try {
        const { id } = res.locals;
        await User.findByIdAndDelete(id);
        res.status(200).json({message: 'Success'});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};

module.exports.changeUserPassword = async (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body;
        const { id } = res.locals;
        const { password } = await User.findById(id);
        if(!(await bcrypt.compare(oldPassword, password))) {
            return res.status(400).json({message: "Your password is invalid. Please try again"});
        }
        await User.findOneAndUpdate({ id }, { password: await bcrypt.hashSync(newPassword, saltRound) });
        res.status(200).json({message: 'Success'});
    } catch (e) {
        res.status(400).json({message: e.message});
    }
};