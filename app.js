require('dotenv').config();
const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const app = express();
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');
const { authMiddleware } = require('./middlewares/authMiddleware');

const { PORT, DB_USER, DB_PASSWORD, DB_HOSTNAME } = require('./config'); 


app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/notes', authMiddleware, noteRouter);


const start = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}${DB_HOSTNAME}/?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    app.listen(PORT, () => {
        console.log('Server started');
    })
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);

function errorHandler (err, _, res, next) {
  console.error(err.message);
  res.status(500).send({'message': 'Server error'});
};