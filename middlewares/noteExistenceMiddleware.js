const { Note } = require('../models/noteModel'); 

module.exports.noteExistenceMiddleware = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { id:userId } = res.locals;
        const note = await Note.findById( id );
        if(userId !== note.userId) {
            return res.status(400).json({message: 'This account doesn\'t have note with this ID!'});
        }
        next();
    } catch (e) {
        return res.status(400).json({message: e.message});
    }
}