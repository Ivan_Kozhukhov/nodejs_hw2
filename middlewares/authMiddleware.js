const jwt = require('jsonwebtoken');
const { SECRET_WORD } = require('../config'); 

module.exports.authMiddleware = async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        if(!authorization) {
            return res.status(400).json({message: 'No Authorization http header found!'});
        }
        const [, token] = authorization.split(' ');
        if(!token) {
            return res.status(400).json({message: 'No JWT token found!'});
        }
        res.locals = jwt.verify(token, SECRET_WORD);
        next();
    } catch (e) {
        return res.status(400).json({message: 'Invalid token'});
    }
}